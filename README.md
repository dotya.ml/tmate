# [`tmate`](https://git.dotya.ml/dotya.ml/tmate)

this repo holds
* `docker-compose.yml` for our instance of [tmate](https://tmate.io)
* `systemd` unit file to easily start/stop/manage the service
* example tmate config to let clients make use of our instance

> Note: commands below are to be run from the project folder

#### server-side setup

```sh
mkdir -pv /etc/tmate
cp -v docker-compose.yml /etc/tmate/
cp -v tmate.service /etc/systemd/system/
systemctl daemon-reload
systemctl enable --now tmate.service
```

#### tmate client setup

```sh
if [ ! -f $HOME/.tmate.conf ]; then
    cp -v example.tmate.conf $HOME/.tmate.conf
else
    printf " * file \"$HOME/.tmate.conf\" already present, not doing anything\n"
fi
```

after you've got the config in place (and tmate installed locally), run `tmate`

